import * as React from 'react'
import {StyleSheet, View} from 'react-native';
import {TextInput} from "react-native";

interface SearchBarProps {
    onChangeHandler: (text:string) => void;
    value: string
}

export class SearchBar extends React.Component<SearchBarProps,any> {

    render() {
        return (
            <View style={styles.row}>
                <TextInput
                    style={styles.searchBar}
                    onChangeText={this.props.onChangeHandler}
                    value={this.props.value}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    searchBar: {
        width: 200,
        borderRadius: 10,
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },
});
