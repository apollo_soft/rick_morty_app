import React, {SFC} from "react";
import {StyleSheet, Text, View, Image} from 'react-native';
import Character from '../../Models/Character';

interface CharacterListItemProps {
    character: Character
}

export const CharacterListItem: SFC<CharacterListItemProps> = (props) => {
    return (
        <View style={styles.row}>
            <Image
                style={itemStyles.image}
                borderRadius={25}
                source={{uri: props.character.image}}
            />
            <View style={styles.column}>
                <Text>{props.character.name}</Text>
                <Text>{props.character.status}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10,
    },
    column: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    image: {
        marginRight: 15,
        width: 50,
        height: 50
    }
});

const itemStyles = StyleSheet.create({
    image: {
        marginRight: 15,
        width: 50,
        height: 50,
    }
});
