export default class Character {
    id: number;
    name: string;
    status: string;
    image: string;
}

