import * as React from 'react'
import {StyleSheet, View, SafeAreaView, FlatList, ListRenderItemInfo, Text, Image} from 'react-native';
import {SearchBar} from "../../Components/SearchBar/SearchBar";
import Character from "../../Models/Character";
import {CharacterListItem} from "../../Components/CharacterListItem/CharacterListItem";
import RickAndMortyHandler from "../../Handlers/RickAndMortyHandler";


interface CharactersState {
    loading: boolean,
    hasMore: boolean,
    query: string,
    page: number,
    charactersList: Character[],
    filterList: Character[]
}

export default class Characters extends React.Component<any, CharactersState> {

    constructor(props: any) {
        super(props);
        this.state = {
            loading: false,
            query: '',
            page: 1,
            hasMore: true,
            charactersList: [],
            filterList: [],
        };
    }

    async handleSearch(text: string) {
        this.setState({
            query: text
        });
        if(text.length > 0 ) {

            try {
                const result = await RickAndMortyHandler.filterCharacters(text);
                console.log(result);
                await this.setState({
                    filterList: result
                });
            } catch (error){
                console.log(error);
            }
        } else {
            await this.setState({
                filterList: []
            });
        }
    }

    componentDidMount() {
        this.loadCharacters();
    }

    async loadNextPage() {
        if(this.state.query.length > 0) {
            if(this.state.hasMore) {
                let newPage = this.state.page + 1;
                console.log('increased to ' + newPage);
                await this.setState({
                    page: this.state.page + 1,
                });
                console.log(this.state);
                await this.loadCharacters();
            }
        }
    }

    async loadCharacters() {
        if(!this.state.loading) {
            this.setState({
                loading: true
            });
            console.log('loading page: ' + this.state.page);
            try {
                const delta: Character[] = await RickAndMortyHandler.getAllCharacters(this.state.page);
                if(delta) {
                    const current: Character[] = [...this.state.charactersList];
                    this.setState({
                        charactersList: current.concat(delta)
                    });
                    console.log(this.state.charactersList);
                    this.setState({
                        loading: false
                    });
                }
            } catch (error) {
                if(error.hasMore != null && !error.hasMore) {
                    this.setState(
                        {
                            loading: false,
                            hasMore: false
                        }
                    );
                }
            }

        }
    }

    render() {
        console.log('should I show the list?');
        console.log((this.state.charactersList.length > 0 && this.state.query.length < 1)  || this.state.filterList.length > 0);

        return(
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff', width: '100%'}}>
                <View style={styles.row}>
                    <Image source={require('../../../assets/images/logo.png')}
                           style={imgStyle.image}
                           resizeMode="contain"/>
                </View>
                <View style={styles.container}>
                    <SearchBar onChangeHandler={this.handleSearch.bind(this)} value={this.state.query}/>
                    { (this.state.charactersList.length > 0 && this.state.query.length < 1)  || this.state.filterList.length > 0 ?
                        <FlatList<Character>
                            style={styles.list}
                            data={this.state.filterList.length > 0 ? this.state.filterList : this.state.charactersList}
                            keyExtractor={(item: Character, _) => item.id.toString()}
                            onEndReached={this.loadNextPage.bind(this)}
                            onEndReachedThreshold={0}
                            renderItem={
                                (item: ListRenderItemInfo<Character>) => <CharacterListItem character={item.item}/>
                            }/>
                        : null
                    }

                    {this.state.loading ?
                        <View style={styles.row}>
                            <Text>Loading</Text>
                        </View>
                        : null}
                    {!this.state.hasMore ?
                        <View style={styles.row}>
                            <Text>There are no more characters</Text>
                        </View>
                        : null}
                    {this.state.query.length > 0 && this.state.filterList.length === 0 ?
                        <View style={styles.row}>
                            <Text>No results</Text>
                        </View>
                        : null}
                </View>
            </SafeAreaView>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width:'100%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    list: {
        marginTop: 10,
    },
    row: {
        marginTop:10,
        width:'100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

const imgStyle = StyleSheet.create({
    image: {
        width: 200,
        height: 50,
    }
});
