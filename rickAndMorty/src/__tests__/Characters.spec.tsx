import * as React from 'react'
import Characters from '../Screens/Characters/Characters';
import { shallow } from 'enzyme';
import {SearchBar} from "../Components/SearchBar/SearchBar";
import {FlatList, Text} from "react-native";


describe('Characters', () => {
    it('should render a SearchBar', () => {
        const wrapper = shallow(<Characters/>);
        const searchBar = wrapper.find(SearchBar);
        expect(searchBar).toHaveLength(1);
    });

    it('shouldnt render a FlatList when data is empty', () => {
        const wrapper = shallow(<Characters/>);
        wrapper.setState({
            charactersList: [],
            filterList: []
        });
        const list = wrapper.find(FlatList);
        expect(list).toHaveLength(0);
    });

    it('should render a FlatList when data isnt empty', () => {
        const wrapper = shallow(<Characters/>);
        wrapper.setState({
            charactersList: [
                {
                    name: 'Dummy',
                    id: '1',
                    status: 'alive',
                    image: 'someurl'
                }
            ]
        });
        const list = wrapper.find(FlatList);
        expect(list).toHaveLength(1);
    });

    it('shouldnt show a list if there is no data', async () => {
        let wrapper = shallow(<Characters/>);
        const list = wrapper.find(FlatList);
        expect(list).toHaveLength(0);
    });

    it('should show last page message when there are no more pages', async () => {
        let wrapper = shallow(<Characters/>);
        wrapper.setState({
            hasMore: false
        });
        expect(wrapper.contains(<Text>There are no more characters</Text>)).toEqual(true);
    });

});
