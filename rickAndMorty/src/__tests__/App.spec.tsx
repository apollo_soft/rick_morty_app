import * as React from 'react'
import App from '../App';
import { shallow } from 'enzyme';
import Characters from "../Screens/Characters/Characters";

describe('App Root', () => {
    it('should render Characters page', () => {
        const wrapper = shallow(<App/>);
        expect(wrapper.contains(<Characters/>)).toEqual(true);
    });
});
