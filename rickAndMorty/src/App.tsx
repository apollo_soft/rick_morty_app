import * as React from 'react'
import {StyleSheet, View} from 'react-native';
import Characters from './Screens/Characters/Characters';
import {Props} from "react";


export default class App extends React.Component<Props<any>> {
  render() {
      return (
        <View style={styles.container}>
            <Characters/>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
      alignItems: 'center',
    backgroundColor: '#fff',
  },
});
