import Character from "../Models/Character";

export default class RickAndMortyHandler {
    static BASE_URL = 'https://rickandmortyapi.com/api/character/';

    static async getAllCharacters (page: number): Promise<Character[]> {
        const url = this.BASE_URL + '?page=' + page ;
       try {
           const result = await fetch(url);
           const jsonResult = await result.json();

           let characterList: Character[] = [];

           if(jsonResult.error) {
               throw {
                   hasMore: false
               };
           } else {
               characterList = jsonResult['results'] as Character[];
           }
           return characterList;
       } catch (error) {
           throw error;
       }
    }

    static async filterCharacters (query: string): Promise<Character[]> {
        const url = this.BASE_URL + '?name=' + query;
        const result = await fetch(url);
        const jsonResult = await result.json();
        let characterList: Character[] = [];
        if(!jsonResult.error) {
            characterList = jsonResult['results'] as Character[];
        }
        return characterList;
    }
}
