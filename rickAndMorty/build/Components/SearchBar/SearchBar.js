import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { TextInput } from "react-native";
export class SearchBar extends React.Component {
    render() {
        return (React.createElement(View, { style: styles.row },
            React.createElement(TextInput, { style: styles.searchBar, onChangeText: this.props.onChangeHandler, value: this.props.value })));
    }
}
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    searchBar: {
        width: 200,
        borderRadius: 10,
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        height: 40,
        borderColor: 'gray',
        borderWidth: 1
    },
});
//# sourceMappingURL=SearchBar.js.map