import React from "react";
import { StyleSheet, Text, View, Image } from 'react-native';
export const CharacterListItem = (props) => {
    return (React.createElement(View, { style: styles.row },
        React.createElement(Image, { style: itemStyles.image, borderRadius: 25, source: { uri: props.character.image } }),
        React.createElement(View, { style: styles.column },
            React.createElement(Text, null, props.character.name),
            React.createElement(Text, null, props.character.status))));
};
const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10,
    },
    column: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    image: {
        marginRight: 15,
        width: 50,
        height: 50
    }
});
const itemStyles = StyleSheet.create({
    image: {
        marginRight: 15,
        width: 50,
        height: 50,
    }
});
//# sourceMappingURL=CharacterListItem.js.map