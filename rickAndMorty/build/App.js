import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import Characters from './Screens/Characters/Characters';
export default class App extends React.Component {
    render() {
        return (React.createElement(View, { style: styles.container },
            React.createElement(Characters, null)));
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});
//# sourceMappingURL=App.js.map