var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export default class RickAndMortyHandler {
    static getAllCharacters(page) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = this.BASE_URL + '?page=' + page;
            try {
                const result = yield fetch(url);
                const jsonResult = yield result.json();
                let characterList = [];
                if (jsonResult.error) {
                    throw {
                        hasMore: false
                    };
                }
                else {
                    characterList = jsonResult['results'];
                }
                return characterList;
            }
            catch (error) {
                throw error;
            }
        });
    }
    static filterCharacters(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = this.BASE_URL + '?name=' + query;
            const result = yield fetch(url);
            const jsonResult = yield result.json();
            let characterList = [];
            if (!jsonResult.error) {
                characterList = jsonResult['results'];
            }
            return characterList;
        });
    }
}
RickAndMortyHandler.BASE_URL = 'https://rickandmortyapi.com/api/character/';
//# sourceMappingURL=RickAndMortyHandler.js.map