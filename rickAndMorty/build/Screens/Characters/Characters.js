var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as React from 'react';
import { StyleSheet, View, SafeAreaView, FlatList, Text, Image } from 'react-native';
import { SearchBar } from "../../Components/SearchBar/SearchBar";
import { CharacterListItem } from "../../Components/CharacterListItem/CharacterListItem";
import RickAndMortyHandler from "../../Handlers/RickAndMortyHandler";
export default class Characters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            query: '',
            page: 1,
            hasMore: true,
            charactersList: [],
            filterList: [],
        };
    }
    handleSearch(text) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setState({
                query: text
            });
            if (text.length > 0) {
                try {
                    const result = yield RickAndMortyHandler.filterCharacters(text);
                    console.log(result);
                    yield this.setState({
                        filterList: result
                    });
                }
                catch (error) {
                    console.log(error);
                }
            }
            else {
                yield this.setState({
                    filterList: []
                });
            }
        });
    }
    componentDidMount() {
        this.loadCharacters();
    }
    loadNextPage() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.state.query.length > 0) {
                if (this.state.hasMore) {
                    let newPage = this.state.page + 1;
                    console.log('increased to ' + newPage);
                    yield this.setState({
                        page: this.state.page + 1,
                    });
                    console.log(this.state);
                    yield this.loadCharacters();
                }
            }
        });
    }
    loadCharacters() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.state.loading) {
                this.setState({
                    loading: true
                });
                console.log('loading page: ' + this.state.page);
                try {
                    const delta = yield RickAndMortyHandler.getAllCharacters(this.state.page);
                    if (delta) {
                        const current = [...this.state.charactersList];
                        this.setState({
                            charactersList: current.concat(delta)
                        });
                        console.log(this.state.charactersList);
                        this.setState({
                            loading: false
                        });
                    }
                }
                catch (error) {
                    if (error.hasMore != null && !error.hasMore) {
                        this.setState({
                            loading: false,
                            hasMore: false
                        });
                    }
                }
            }
        });
    }
    render() {
        console.log('should I show the list?');
        console.log((this.state.charactersList.length > 0 && this.state.query.length < 1) || this.state.filterList.length > 0);
        return (React.createElement(SafeAreaView, { style: { flex: 1, backgroundColor: '#fff', width: '100%' } },
            React.createElement(View, { style: styles.row },
                React.createElement(Image, { source: require('../../../assets/images/logo.png'), style: imgStyle.image, resizeMode: "contain" })),
            React.createElement(View, { style: styles.container },
                React.createElement(SearchBar, { onChangeHandler: this.handleSearch.bind(this), value: this.state.query }),
                (this.state.charactersList.length > 0 && this.state.query.length < 1) || this.state.filterList.length > 0 ?
                    React.createElement(FlatList, { style: styles.list, data: this.state.filterList.length > 0 ? this.state.filterList : this.state.charactersList, keyExtractor: (item, _) => item.id.toString(), onEndReached: this.loadNextPage.bind(this), onEndReachedThreshold: 0, renderItem: (item) => React.createElement(CharacterListItem, { character: item.item }) })
                    : null,
                this.state.loading ?
                    React.createElement(View, { style: styles.row },
                        React.createElement(Text, null, "Loading"))
                    : null,
                !this.state.hasMore ?
                    React.createElement(View, { style: styles.row },
                        React.createElement(Text, null, "There are no more characters"))
                    : null,
                this.state.query.length > 0 && this.state.filterList.length === 0 ?
                    React.createElement(View, { style: styles.row },
                        React.createElement(Text, null, "No results"))
                    : null)));
    }
    ;
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    list: {
        marginTop: 10,
    },
    row: {
        marginTop: 10,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
});
const imgStyle = StyleSheet.create({
    image: {
        width: 200,
        height: 50,
    }
});
//# sourceMappingURL=Characters.js.map