var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as React from 'react';
import Characters from '../Screens/Characters/Characters';
import { shallow } from 'enzyme';
import { SearchBar } from "../Components/SearchBar/SearchBar";
import { FlatList, Text } from "react-native";
describe('Characters', () => {
    it('should render a SearchBar', () => {
        const wrapper = shallow(React.createElement(Characters, null));
        const searchBar = wrapper.find(SearchBar);
        expect(searchBar).toHaveLength(1);
    });
    it('shouldnt render a FlatList when data is empty', () => {
        const wrapper = shallow(React.createElement(Characters, null));
        wrapper.setState({
            charactersList: [],
            filterList: []
        });
        const list = wrapper.find(FlatList);
        expect(list).toHaveLength(0);
    });
    it('should render a FlatList when data isnt empty', () => {
        const wrapper = shallow(React.createElement(Characters, null));
        wrapper.setState({
            charactersList: [
                {
                    name: 'Dummy',
                    id: '1',
                    status: 'alive',
                    image: 'someurl'
                }
            ]
        });
        const list = wrapper.find(FlatList);
        expect(list).toHaveLength(1);
    });
    it('shouldnt show a list if there is no data', () => __awaiter(this, void 0, void 0, function* () {
        let wrapper = shallow(React.createElement(Characters, null));
        const list = wrapper.find(FlatList);
        expect(list).toHaveLength(0);
    }));
    it('should show last page message when there are no more pages', () => __awaiter(this, void 0, void 0, function* () {
        let wrapper = shallow(React.createElement(Characters, null));
        wrapper.setState({
            hasMore: false
        });
        expect(wrapper.contains(React.createElement(Text, null, "There are no more characters"))).toEqual(true);
    }));
});
//# sourceMappingURL=Characters.spec.js.map